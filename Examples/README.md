## Jupyter Notebook example

**NOTE** The SVGs displayed by Jupyter don't play well with the GitLab renderer, it is best to run the notebook locally. You should get something that looks more like this:  
![](screenshot.png)

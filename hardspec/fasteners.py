from hardspec.materials import Material,materials
from copy import copy,deepcopy
import os

class Thread:
    '''Class for threads. Class defines all pitches for a given defined
    metric thread as specified in ISO 261
    '''
    
    def __init__(self,d,coarse=None,fine=[],note=None,pitch_notes={}):
        
        valid_pitches = [.2,.25,0.3,.35,.4,.45,.5,.6,.7,.75,.8,1,1.25,1.5,1.75,2,2.5,3,3.5,4,4.5,5,5.5,6,8]
        assert isinstance(d,(float,int)), 'd must be set as a float or integer'
        assert d>0, 'Diameter must be positive'
        if isinstance(d,int):
            self._nom_diameter = d
        elif d.is_integer():
            self._nom_diameter = int(d)
        else:
            assert round(d*10)/10 == d, 'd must be set to nearest 0.1 mm'
            self._nom_diameter = d
        
        assert (coarse is None) or isinstance(coarse,(float,int)),'coarse pitch must be set as a float or integer'
        self._coarse = coarse
        
        assert isinstance(fine,(list,float,int))
        if isinstance(fine,list):
            self._fine = fine
        else:
            self._fine = [fine]
        
        assert bool(self._fine) or bool(self._coarse), 'Either coarse or fine must be set'
        
        if self._coarse:
            assert max(self._fine)<self._coarse
            self._all_pitches = [self._coarse]
        else:
            self._all_pitches = []
        
        if self._fine:
            self._all_pitches +=self._fine
    
        for n in range(len(self._all_pitches)):
            assert self._all_pitches[n]>0, 'Pitches must be positive'
            assert self._all_pitches[n] in valid_pitches, f'{self._all_pitches[n]} is not a valid pitch according to ISO 261'
            if isinstance(self._all_pitches[n],float) and self._all_pitches[n].is_integer():
                self._all_pitches[n] = int(self._all_pitches[n])
                
        if self._nom_diameter <= 20:
            #Note ISO 261 gives no maximum siameter for .2,.25, or .35 , but gives M22 for .5
            # for this reason we give M20 for the lower ones. Probably overly permissive
            fine_pitches = [3,2,1.5,1,.75,.5,.35,.25,.2]
        elif self._nom_diameter <= 22:
            fine_pitches = [3,2,1.5,1,.75,.5]
        elif self._nom_diameter <= 33:
            fine_pitches = [3,2,1.5,1,.75]
        elif self._nom_diameter <= 80:
            fine_pitches = [3,2,1.5,1]
        elif self._nom_diameter <= 150:
            fine_pitches = [3,2,1.5]
        elif self._nom_diameter <= 200:
            fine_pitches = [3,2]
        elif self._nom_diameter <= 300:
            fine_pitches = [3]
        
        #Allowed pitches which are finder than defined fine pitches according to ISO 261,
        self._allowed_pitches = self._all_pitches + [p for p in fine_pitches if p< self._all_pitches[-1]]
        
        if note is not None:
            assert isinstance(note,str)
        self._note=note
        
        assert isinstance(pitch_notes,dict), f'Pitch_notes must be dictionaries not {type(pitch_notes)}'
        self._pitch_notes = pitch_notes
        for key in pitch_notes.keys():
            pitch = float(key)
            assert pitch in self._all_pitches, 'Cannot write note about undefined pitch'
            
    def __str__(self):
        if len(self._all_pitches) > 1:
            output = '['
        else:
            output = ''
        for n,pitch in enumerate(self._all_pitches):
            if n != 0:
                output += ', '
            output += f'M{self._nom_diameter}-{pitch}'
            if str(pitch) in self._pitch_notes:
                output += f' ({self._pitch_notes[str(pitch)]})'
        if len(self._all_pitches) > 1:
            output += ']'
        if self._note:
            output += f' ({self._note})'
        return output
    
    def __repr__(self):
        return 'Metric Thread: '+str(self)
    
    def __eq__(self,other):
        if isinstance(other,(float,int)):
            return other == self._nom_diameter
        elif isinstance(other,(list,tuple)):
            if len(other) == 2:
                return other[0] == self._nom_diameter and other[1] in self._all_pitches
            else:
                return False
        elif isinstance(other,Thread):
            return self._nom_diameter == other._nom_diameter and self._all_pitches == other._all_pitches 
        else:
            return False
    
    @property
    def nominal_diameter(self):
        return self._nom_diameter
    
    @property
    def coarse_pitch(self):
        return self._coarse
    
    @property
    def all_pitches(self):
        return deepcopy(self._all_pitches)
    
    @property
    def allowed_pitches(self):
        return deepcopy(self._allowed_pitches)
    
    def defined_pitch(self,p):
        return p in self._all_pitches
    
    def allowed_pitch(self,p):
        return p in self._allowed_pitches
    
    def has_coarse(self):
        return self._coarse is not None
    
    def has_fine(self):
        return bool(self._fine)
        
            

class Standard:
    '''Generic class for information defined in a standard (such as an ISO
    standard) used for information from a standard that doesn't fit in another
    class such as "MetricFastener".
    Rather than make an instance this sould be subclassed for new standards
    '''

    def __init__(self):
        self._description = ''
        self._standard = ''
        self._standard_full_name = ''
        
    def __str__(self):
        return f'{self._description} As defined in {self._standard} ({self._standard_full_name}).' 
    
    def __repr__(self):
        return f'Standard - {self._description}'

    
class MetricScrewThreads(Standard):
    
    def __init__(self):
        self._description = 'Metric Screw Threads'
        self._standard = 'ISO 261'
        self._standard_full_name = 'BS ISO 261:1998'
        self._first_choice = [Thread(1,    0.25,  .2),
                              Thread(1.2,  0.25,  .2),
                              Thread(1.6,  0.35,  .2),
                              Thread(2,    0.4,   .25),
                              Thread(2.5,  0.45,  .35),
                              Thread(3,    0.5,   .35),
                              Thread(4,    0.7,   .5),
                              Thread(5,    0.8,   .5),
                              Thread(6,    1,     .75),
                              Thread(8,    1.25,  [1,0.75]),
                              Thread(10,   1.5,   [1.25,1,0.75]),
                              Thread(12,   1.75,  [1.5,1.25,1]),
                              Thread(16,   2,     [1.5,1]),
                              Thread(20,   2.5,   [2,1.5,1]),
                              Thread(24,   3,     [2,1.5,1]),
                              Thread(30,   3.5,   [3,2,1.5,1],pitch_notes={'3':'To be avoided as far as possible.'}),
                              Thread(36,   4,     [3,2,1.5]),
                              Thread(42,   4.5,   [4,3,2,1.5]),
                              Thread(48,   5,     [4,3,2,1.5]),
                              Thread(56,   5.5,   [4,3,2,1.5]),
                              Thread(64,   6,     [4,3,2,1.5]),
                              Thread(72,   fine = [6,4,3,2,1.5]),
                              Thread(80,   fine = [6,4,3,2,1.5]),
                              Thread(90,   fine = [6,4,3,2]),
                              Thread(100,  fine = [6,4,3,2]),
                              Thread(110,  fine = [6,4,3,2]),
                              Thread(125,  fine = [8,6,4,3,2]),
                              Thread(140,  fine = [8,6,4,3,2]),
                              Thread(160,  fine = [8,6,4,3]),
                              Thread(180,  fine = [8,6,4,3]),
                              Thread(200,  fine = [8,6,4,3]),
                              Thread(220,  fine = [8,6,4,3]),
                              Thread(250,  fine = [8,6,4,3]),
                              Thread(280,  fine = [8,6,4])
                             ]
        self._second_choice = [Thread(1.1,  0.25,  .2),
                               Thread(1.4,  0.3,   .2),
                               Thread(1.8,  0.35,  .2),
                               Thread(2.2,  0.45,  .25),
                               Thread(3.5,  0.6,   .35),
                               Thread(4.5,  0.75,  .5),
                               Thread(7,    1,     .75),
                               Thread(14,   2,     [1.5,1.25,1],pitch_notes={'1.25':'Only for spark plugs for engines.'}),
                               Thread(18,   2.5,   [2,1.5,1]),
                               Thread(22,   2.5,   [2,1.5,1]),
                               Thread(27,   3,     [2,1.5,1]),
                               Thread(33,   3.5,   [3,2,1.5],pitch_notes={'3':'To be avoided as far as possible.'}),
                               Thread(39,   4,     [3,2,1.5]),
                               Thread(45,   4.5,   [4,3,2,1.5]),
                               Thread(52,   5,     [4,3,2,1.5]),
                               Thread(60,   5.5,   [4,3,2,1.5]),
                               Thread(68,   6,     [4,3,2,1.5]),
                               Thread(76,   fine = [6,4,3,2,1.5]),
                               Thread(85,   fine = [6,4,3,2]),
                               Thread(95,   fine = [6,4,3,2]),
                               Thread(105,  fine = [6,4,3,2]),
                               Thread(115,  fine = [6,4,3,2]),
                               Thread(120,  fine = [6,4,3,2]),
                               Thread(130,  fine = [8,6,4,3,2]),
                               Thread(150,  fine = [8,6,4,3,2]),
                               Thread(170,  fine = [8,6,4,3]),
                               Thread(190,  fine = [8,6,4,3]),
                               Thread(210,  fine = [8,6,4,3]),
                               Thread(240,  fine = [8,6,4,3]),
                               Thread(260,  fine = [8,6,4]),
                               Thread(300,  fine = [8,6,4])
                              ]
        self._third_choice = [Thread(5.5,  fine = 0.5),
                              Thread(9,    1.25,  [1,.75]),
                              Thread(11,   1.5,   [1,.75]),
                              Thread(15,   fine = [1.5,1]),
                              Thread(17,   fine = [1.5,1]),
                              Thread(25,   fine = [2,1.5,1]),
                              Thread(26,   fine = 1.5),
                              Thread(28,   fine = [2,1.5,1]),
                              Thread(32,   fine = [2,1.5]),
                              Thread(35,   fine = 1.5,note='Only for locking nuts for bearings.'),
                              Thread(38,   fine = 1.5),
                              Thread(40,   fine = [3,2,1.5]),
                              Thread(50,   fine = [3,2,1.5]),
                              Thread(55,   fine = [4,3,2,1.5]),
                              Thread(58,   fine = [4,3,2,1.5]),
                              Thread(62,   fine = [4,3,2,1.5]),
                              Thread(65,   fine = [4,3,2,1.5]),
                              Thread(70,   fine = [6,4,3,2,1.5]),
                              Thread(75,   fine = [4,3,2,1.5]),
                              Thread(78,   fine = 2),
                              Thread(82,   fine = 2),
                              Thread(135,  fine = [6,4,3,2]),
                              Thread(145,  fine = [6,4,3,2]),
                              Thread(155,  fine = [6,4,3]),
                              Thread(165,  fine = [6,4,3]),
                              Thread(175,  fine = [6,4,3]),
                              Thread(185,  fine = [6,4,3]),
                              Thread(195,  fine = [6,4,3]),
                              Thread(205,  fine = [6,4,3]),
                              Thread(215,  fine = [6,4,3]),
                              Thread(225,  fine = [6,4,3]),
                              Thread(230,  fine = [8,6,4,3]),
                              Thread(235,  fine = [6,4,3]),
                              Thread(245,  fine = [6,4,3]),
                              Thread(255,  fine = [6,4]),
                              Thread(265,  fine = [6,4]),
                              Thread(270,  fine = [8,6,4]),
                              Thread(275,  fine = [6,4]),
                              Thread(285,  fine = [6,4]),
                              Thread(290,  fine = [8,6,4]),
                              Thread(295,  fine = [6,4])
                             ]
        
        self._all_threads = self._first_choice+self._second_choice+self._third_choice
        self._all_threads.sort(key=lambda thread: thread.nominal_diameter)
        
    def is_first_choice(self,thread):
        return thread in self._first_choice
    
    def is_second_choice(self,thread):
        return thread in self._second_choice
    
    def is_third_choice(self,thread):
        return thread in self._third_choice
    
    def is_defined(self,thread):
        return thread in self._all_threads
    
    def is_allowed(self,thread):
        assert isinstance(thread,(list,tuple)), 'Thread should be a list or tuple of diameter and pitch in mm'
        assert len(thread) ==2, 'Thread should be a list or tuple of diameter and pitch in mm'
        for t in self._all_threads:
            if t==thread[0] and t.allowed_pitch(thread[1]):
                return True
        return False
    
    @property
    def first_choice_threads(self):
        return deepcopy(self._first_choice)
    
    @property
    def second_choice_threads(self):
        return deepcopy(self._second_choice)
        
    @property
    def third_choice_threads(self):
        return deepcopy(self._third_choice)
        
    @property
    def all_threads(self):
        return deepcopy(self._all_threads)
    
    @property
    def first_choice_diameters(self):
        return [t.nominal_diameter for t in self._first_choice]
    
    @property
    def second_choice_diameters(self):
        return [t.nominal_diameter for t in self._second_choice]
        
    @property
    def third_choice_diameters(self):
        return [t.nominal_diameter for t in self._third_choice]
        
    @property
    def all_diameters(self):
        return [t.nominal_diameter for t in self._all_threads]
    
    def get_coarse_pitch(self,d):
        try:
            return self._all_threads[self._all_threads.index(d)].coarse_pitch
        except ValueError:
            return None
    
    def get_pitches(self,d):
        try:
            return deepcopy(self._all_threads[self._all_threads.index(d)].all_pitches)
        except ValueError:
            return None
    
    def get_allowed_pitches(self,d):
        try:
            return deepcopy(self._all_threads[self._all_threads.index(d)].allowed_pitches)
        except ValueError:
            return None
    
class SelectedThreads(Standard):
    '''Selected thread diameters for general engineering (ISO 262). This is a subset
    of the threads defined in (ISO 261)'''
    
    def __init__(self):
        self._description = 'Selected metric thread diameters.'
        self._first_choice = [(1,   0.25),
                              (1.2, 0.25),
                              (1.6, 0.35),
                              (2,   0.4),
                              (2.5, 0.45),
                              (3,   0.5),
                              (4,   0.7),
                              (5,   0.8),
                              (6,   1),
                              (8,   1.25),
                              (10,  1.5),
                              (12,  1.75),
                              (16,  2),
                              (20,  2.5),
                              (24,  3),
                              (30,  3.5),
                              (36,  4),
                              (42,  4.5),
                              (48,  5),
                              (56,  5.5),
                              (64,  6)
                             ]
        self._second_choice = [(1.4, 0.3),
                               (1.8, 0.35),
                               (3.5, 0.6),
                               (7,   1),
                               (14,  2),
                               (18,  2.5),
                               (22,  2.5),
                               (27,  3),
                               (33,  3.5),
                               (39,  4),
                               (45,  4.5),
                               (52,  5),
                               (60,  5.5)
                              ]
        self._first_choice_fine = [(8,   1),
                                   (10,  1.25),
                                   (10 , 1),
                                   (12,  1.5),
                                   (12,  1.25),
                                   (16,  1.5),
                                   (20,  2),
                                   (20,  1.5),
                                   (24,  2),
                                   (30,  2),
                                   (36,  3),
                                   (42,  3),
                                   (48,  3),
                                   (56,  4),
                                   (64,  4)
                                  ]
        self._second_choice_fine = [(14,  1.5),
                                    (18,  2),
                                    (18,  1.5),
                                    (22,  2),
                                    (22,  1.5),
                                    (27,  2),
                                    (33,  2),
                                    (39,  3),
                                    (45,  3),
                                    (52,  4),
                                    (60,  4)
                                   ]
        
        
        
        self._standard = 'ISO 262'
        self._standard_full_name = 'BS ISO 262:1998'
        
    def is_selected_diameter(self,d,include_second_choice=False):
        if d in [thread[0] for thread in self._first_choice]:
            return True
        elif include_second_choice and d in [thread[0] for thread in self._second_choice]:
            return True
        else:
            return False
        
    def get_threads(self,include_second_choice=False,include_fine=False):
        threads = deepcopy(self._first_choice)
        if include_second_choice:
            threads += deepcopy(self._second_choice)
            if include_fine:
                threads += deepcopy(self._first_choice_fine)
                threads += deepcopy(self._second_choice_fine)
        else:
            if include_fine:
                threads += deepcopy(self._first_choice_fine)
        return sorted(threads)
    
    def get_diameters(self,include_second_choice=False):
        ds = [t[0] for t in self._first_choice]
        if include_second_choice:
            ds += [t[0] for t in self._second_choice]
        return sorted(ds)
        
    def is_selected(self,d,p,include_second_choice=False,include_fine=False):
        return [d,p] in self.get_threads(include_second_choice=include_second_choice,include_fine=include_fine)
        
class ScrewLengths(Standard):
    
    def __init__(self):
        self._description = 'Metric preferred screw lenghts.'
        self._first_choice = [2,3,4,5,6,8,10,12,16,20,25,30,35,40,45,50,
                              55,60,65,70,80,90,100,110,120,130,140,150,
                              160,180,200,220,240,260,280,300,320,340,360,
                              400,420,440,460,480,500]

        self._second_choice = [2.5,7,9,11,14,18,22,28,32,38,75,85,95,105,
                               115,125,170,190]
        self._standard = 'ISO 888'
        self._standard_full_name = 'BS EN ISO 888:2018'
    
    def is_preferred(self,l,include_second_choice=False):
        if l in self._first_choice:
            return True
        elif include_second_choice and l in self._second_choice:
            return True
        else:
            return False
    
    @property
    def first_choice_lengths(self):
        return copy(self._first_choice)
    
    def second_choice_lengths(self):
        return copy(self._second_choice)
        
    def all_lengths(self):
        return self._first_choice+self._second_choice
    
    def for_diameter(self,d):
        '''Lengths for screws of each diameter as a subset of first choices specified by
        ISO 888:2018
        No specific standards found, this is just a guide for what seems to exist.
        Shortest
        ISO 4762:2004 seems to have them start at 1.5*d, but some sites sell M20x25.
        As M20x25 is one size smaller than 1.5d, we search for the nearest size
        smaller but not equal to 1.5d
        Longest
        ISO 4762:2004 seems to go to d*10, but accu.co.uk goes much further including
        M3x60 (d*20) or M6x150 (d*25). We search up to d*25

        Output is the list of lengths in mm'''
        len_list = [l for l in self._first_choice if (l>=1.5*d and l<=25*d)]
        ind = self._first_choice.index(len_list[0])
        len_list.insert(0,self._first_choice[ind-1])
        return len_list

#Instances of the standards
metric_threads = MetricScrewThreads()
selected_threads = SelectedThreads()
screw_lengths = ScrewLengths()

class MetricFastener:
    '''Generic class for metric fasteners with ISO 262 threads or related hardware
    (such as washers) for these fasteners.
    '''
    
    def __init__(self,d,material='SS',left_hand=False):
        
        assert isinstance(d,(float,int)), 'd must be set as a float or integer'
        if isinstance(d,int):
            self._nom_diameter = d
        elif d.is_integer():
            self._nom_diameter = int(d)
        else:
            assert round(d*10)/10 == d, 'd must be set to nearest 0.1 mm'
            self._nom_diameter = d
        
        self._standard_diameter = selected_threads.is_selected_diameter(self._nom_diameter)
        if self._standard_diameter:
            self._second_choice_diameter = False
        else:
            self._second_choice_diameter = selected_threads.is_selected_diameter(self._nom_diameter,
                                                                                 include_second_choice=True)
        
        assert isinstance(material,(Material,str)), 'material must be a string or a Material Object'
        if isinstance(material,Material):
            self._material = material
        else:
            if material in materials:
                self._material = materials[materials.index(material)]
        self._standard = None
        self._standard_full_name = None
        self._alternative_standards = []
        self._product_name = 'Undefined Fastener'
        
        self._image = None

    def __str__(self):
        return f'M{self._nom_diameter} '+self._product_name + f' in {self._material}'

    def __repr__(self):
        return str(self)
    
    @property
    def dimensions(self):
        return self.get_dimensions()
    
    def get_dimensions():
        return {}
    
    @property
    def image(self):
        return copy(self._image)

class MetricScrew(MetricFastener):
    
    def __init__(self,d,l,material='SS',left_hand=False,full_thread=False):
        
        super().__init__(d,material=material,left_hand=left_hand)
        self._product_name = 'Undefined Screw'
        self._length = l
        
        self._standard_length = screw_lengths.is_preferred(self._length)
        if self._standard_length:
            self._second_choice_length = False
        else:
            self._second_choice_length = screw_lengths.is_preferred(self._length,
                                                                    include_second_choice=True)
        
        if full_thread:
            self._thread_length = l
            self._full_thread = True
        else:
            self._calc_thread_length()
        
            
    def __str__(self):
        return f'M{self._nom_diameter}x{self._length}mm '+self._product_name + f' in {self._material}'

    def _calc_thread_length(self):
        '''Thread length, b, Calculations for screws of nominal diameter d
        and length l
        From ISO 888:2018
        l <= 125mm: b = 2d + 6 mm
        l > 125 mm and l<= 200 mm:  b = 2d + 12 mm
        l > 200 mm:  b = 2d + 25 mm
        If bolt is short enough that unthreaded portion is less than 0.5d,
        then bolt is fully threaded.
        '''
        
        if self._length <= 125:
            b = round(2*self._nom_diameter + 6)
        elif self._length<= 200:
            b = round(2*self._nom_diameter + 12)
        else:
            b = round(2*self._nom_diameter + 25)

        if self._length-b < 0.5*self._nom_diameter:
            full_thread = True
            b=self._length
        else:
            full_thread = False
        
        self._thread_length = b
        self._full_thread = full_thread
        
    def get_dimensions(self):
        dimensions = {"Nominal length (l)":self._length,'Nominal diameter (d)':self._nom_diameter,'Thread pitch':metric_threads.get_coarse_pitch(self._nom_diameter)}
        if dimensions['Thread pitch'] is None:
            dimensions['Thread pitch'] = "Not specified"
        if not self._full_thread:
            dimensions["Thread length (b)"] = self._thread_length
        return dimensions
    
    @property
    def fully_threaded(self):
        return self._full_thread


class HexBolt(MetricScrew):
    # ISO 4017  - Full thread hex bolt (previously DIN 933)
    # ISO 4014  - Partial thread hex bolt (previously DIN 931)
    def __init__(self,d,l,material='SS',left_hand=False,full_thread=False):
        
        super().__init__(d,l,material=material,left_hand=left_hand,full_thread=full_thread)
        
        if self._full_thread:
            self._standard = "ISO 4017"
            self._standard_full_name = "BS EN ISO 4017:2014"
            self._alternative_standards = ["DIN 933"]
            self._product_name = 'Hex Bolt (Fully threaded)'
            with open(os.path.join(os.path.dirname(__file__),'includes','HexBoltFullThread.svg'),'r') as fid:
                self._image = fid.read()
        else:
            self._standard = "ISO 4014"
            self._standard_full_name = "BS EN ISO 4014:2011"
            self._alternative_standards = ["DIN 931"]
            self._product_name = 'Hex Bolt (Partially threaded)'
            with open(os.path.join(os.path.dirname(__file__),'includes','HexBoltPartialThread.svg'),'r') as fid:
                self._image = fid.read()

    def get_dimensions(self):
        
        dimensions = super().get_dimensions()
        # Nominal drive width from one flat to another
        drive_widths = {1.6:3.2,
                        2:4,
                        2.5:5,
                        3:5.5,
                        3.5:6,
                        4:7,
                        5:8,
                        6:10,
                        8:13,
                        10:16,
                        12:18,
                        14:21,
                        16:24,
                        18:27,
                        20:30,
                        22:34,
                        24:36,
                        27:41,
                        30:46,
                        33:50,
                        36:55,
                        39:60,
                        42:65,
                        45:70,
                        48:75,
                        52:80,
                        56:85,
                        60:90,
                        64:95}
        
        head_depths = {1.6:1.1,
                       2:1.4,
                       2.5:1.7,
                       3:2,
                       3.5:2.4,
                       4:2.8,
                       5:3.5,
                       6:4,
                       8:5.3,
                       10:6.4,
                       12:7.5,
                       14:8.8,
                       16:10,
                       18:11.5,
                       20:12.5,
                       22:14,
                       24:15,
                       27:17,
                       30:18.7,
                       33:21,
                       36:22.5,
                       39:25,
                       42:26,
                       45:28,
                       48:30,
                       52:33,
                       56:35,
                       60:38,
                       64:40}
        
        if self._nom_diameter in drive_widths and self._nom_diameter in head_depths:
            dimensions["Drive width (s)"] = drive_widths[self._nom_diameter]
            dimensions["Head depth (k)"] = head_depths[self._nom_diameter]
        else:
            dimensions["Drive width (s)"] = "Not specified"
            dimensions["Head depth (k)"] = "Not specified"
        return dimensions
            

class SocketHeadScrew(MetricScrew):
    '''Note this is an undefined type of socket head screw such as a Hexagon Socket
    Head Cap Scew, or a Hexagon Socket Head Button Screw, as these have different
    partial thread lengths to metric screws defined. Also this has no option to force full
    thread'''
    
    def __init__(self,d,l,material='SS',left_hand=False):
        
        super().__init__(d,l,material=material,left_hand=left_hand,full_thread=False)
        self._product_name = 'Undefined Socket Head Screw'
    
    def _calc_thread_length(self):
        '''Thread length, b, Calculations for screws of nominal diameter d
        and length l
        
        b = 2d + 12 mm
        If bolt is short enough that unthreaded portion is less than d,
        then bolt is fully threaded.
        '''
        
        b = round(2*self._nom_diameter + 12)
        
        '''There seems to be no fixed formula for when bolts start being partially
        threaded, it is about l-b is > diameter but there are exceptions.
        Reference: EN ISO 4762:2004'''
        
        #non-conforming diameters
        nc_diams = [2.5,4,12,14,36,56,64]
        #the length where they become full threaded
        nc_diams_lens = [25, 30, 55, 60, 120, 180, 200]
        if self._nom_diameter in nc_diams:
            
            full_below = nc_diams_lens[nc_diams.index(self._nom_diameter)]
            if self._length < full_below:
                full_thread = True
                b=self._length
            else:
                full_thread = False
        elif self._length-b <= self._nom_diameter:
            full_thread = True
            b=self._length
        else:
            full_thread = False
        
        self._thread_length = b
        self._full_thread = full_thread
    

class CapScrew(SocketHeadScrew):
    # ISO 4762 - Socket head cap screw
    def __init__(self,d,l,material='SS',left_hand=False):
        
        super().__init__(d,l,material=material,left_hand=left_hand)
        
        
        self._standard = "ISO 4762"
        self._standard_full_name = "EN ISO 4762:2004"
        self._alternative_standards = ["DIN 912"]
        self._product_name = 'Socket Head Cap Screw'
        if self._full_thread:
            with open(os.path.join(os.path.dirname(__file__),'includes','SocketHeadCapScrewFullThread.svg'),'r') as fid:
                self._image = fid.read()
        else:
            with open(os.path.join(os.path.dirname(__file__),'includes','SocketHeadCapScrewPartialThread.svg'),'r') as fid:
                self._image = fid.read()

    def get_dimensions(self):
        dimensions = super().get_dimensions()
        # Nominal drive width from one flat to another
        drive_widths = {1.6:1.5,
                        2:1.5,
                        2.5:2,
                        3:2.5,
                        4:3,
                        5:4,
                        6:5,
                        8:6,
                        10:8,
                        12:10,
                        14:12,
                        16:14,
                        20:17,
                        24:19,
                        30:22,
                        36:27,
                        42:32,
                        48:36,
                        56:41,
                        64:46}
        
        #Note these are the maximum/nominal diameter for a smooth cap screw
        #Knurled heads can be slightly larger.
        head_diameters = {1.6:3,
                          2:3.8,
                          2.5:4.5,
                          3:5.5,
                          4:7,
                          5:8.5,
                          6:10,
                          8:13,
                          10:16,
                          12:18,
                          14:21,
                          16:24,
                          20:30,
                          24:36,
                          30:45,
                          36:54,
                          42:63,
                          48:72,
                          56:84,
                          64:96}
        
        if self._nom_diameter in drive_widths and self._nom_diameter in head_diameters:
            dimensions["Drive width (s)"] = drive_widths[self._nom_diameter]
            dimensions["Head diameter (d_k)"] = head_diameters[self._nom_diameter]
        else:
            dimensions["Drive width (s)"] = "Not specified"
            dimensions["Head diameter (d_k)"] = "Not specified"
        dimensions["Head depth (k)"] = self._nom_diameter
        return dimensions



class ButtonScrew(SocketHeadScrew):
    # ISO 7380 - Socket Head Button Screws
    def __init__(self,d,l,material='SS',left_hand=False):
        
        super().__init__(d,l,material=material,left_hand=left_hand)
        
        
        self._standard = "ISO 7380-1"
        self._standard_full_name = "BS EN ISO 7380-1:2011"
        self._alternative_standards = ["ISO 7380"]
        self._product_name = 'Socket Head Button Screw'
        if self._full_thread:
            with open(os.path.join(os.path.dirname(__file__),'includes','SocketHeadButtonScrewFullThread.svg'),'r') as fid:
                self._image = fid.read()
        else:
            with open(os.path.join(os.path.dirname(__file__),'includes','SocketHeadButtonScrewPartialThread.svg'),'r') as fid:
                self._image = fid.read()

    def get_dimensions(self):
        dimensions = super().get_dimensions()
        # Nominal drive width from one flat to another
        drive_widths = {3:2,
                        4:2.5,
                        5:3,
                        6:4,
                        8:5,
                        10:6,
                        12:8,
                        16:10}
        
        #Note these are the maximum/nominal diameter
        head_diameters = {3:5.7,
                          4:7.6,
                          5:9.5,
                          6:10.5,
                          8:14,
                          10:17.5,
                          12:21,
                          16:28}
        #Diameter of flat section on top of screw (ref. value)
        flat_diameters = {3:2.6,
                          4:3.8,
                          5:6,
                          6:6,
                          8:7.7,
                          10:10,
                          12:12,
                          16:16}
        
        head_depths = {3:1.65,
                       4:2.20,
                       5:2.75,
                       6:3.30,
                       8:4.40,
                       10:5.50,
                       12:6.60,
                       16:8.80}

        
        if self._nom_diameter in drive_widths:
            dimensions["Drive width (s)"] = drive_widths[self._nom_diameter]
            dimensions["Head diameter (d_k)"] = head_diameters[self._nom_diameter]
            dimensions["Head depth (k)"] = head_depths[self._nom_diameter]
            dimensions["Flat diameter (d_L)"] = flat_diameters[self._nom_diameter]
        else:
            dimensions["Drive width (s)"] = "Not specified"
            dimensions["Head diameter (d_k)"] = "Not specified"
            dimensions["Head depth (k)"] = "Not specified"
            dimensions["Flat diameter (d_L)"] = "Not specified"
        return dimensions


### NUTS, WASHERS ETC

class HexNut(MetricFastener):
    # ISO 4032 - Hex nut (previously DIN 934) - Note fine thread are ISO 8673 but this is often ignored
    
    def __init__(self,d,material='SS',left_hand=False):
        
        super().__init__(d,material=material,left_hand=left_hand)
        
        self._standard = "ISO 4032"
        self._standard_full_name = "BS EN ISO 4032:2012"
        self._alternative_standards = ["DIN 934"]
        self._product_name = 'Hex Nut'
        with open(os.path.join(os.path.dirname(__file__),'includes','HexNut.svg'),'r') as fid:
            self._image = fid.read()
    
    def get_dimensions(self):
        # Nominal drive width from one flat to another
        drive_widths = {1.6:3.2,
                        2:4,
                        2.5:5,
                        3:5.5,
                        3.5:6,
                        4:7,
                        5:8,
                        6:10,
                        8:13,
                        10:16,
                        12:18,
                        14:21,
                        16:24,
                        18:27,
                        20:30,
                        22:34,
                        24:36,
                        27:41,
                        30:46,
                        33:50,
                        36:55,
                        39:60,
                        42:65,
                        45:70,
                        48:75,
                        52:80,
                        56:85,
                        60:90,
                        64:95}
        
        thickness = {1.6:1.30,
                     2:1.60,
                     2.5:2.00,
                     3:2.40,
                     3.5:2.80,
                     4:3.20,
                     5:4.70,
                     6:5.20,
                     8:6.80,
                     10:8.40,
                     12:10.80,
                     14:12.80,
                     16:14.80,
                     18:15.80,
                     20:18.00,
                     22:19.40,
                     24:21.50,
                     27:23.80,
                     30:25.60,
                     33:28.70,
                     36:31.00,
                     39:33.40,
                     42:34.00,
                     45:36.00,
                     48:38.00,
                     52:42.00,
                     56:45.00,
                     60:48.00,
                     64:51.00}
        
        
        dimensions = {'Nominal thread diameter':self._nom_diameter,'Thread pitch':metric_threads.get_coarse_pitch(self._nom_diameter)}
        if dimensions['Thread pitch'] is None:
            dimensions['Thread pitch'] = "Not specified"
        if self._nom_diameter in drive_widths and self._nom_diameter in thickness:
            dimensions["Drive width (s)"] = drive_widths[self._nom_diameter]
            dimensions["Thickness (m)"] = thickness[self._nom_diameter]
        else:
            dimensions["Drive width (s)"] = "Not specified"
            dimensions["Thickness (m)"] = "Not specified"
        return dimensions




class Washer(MetricFastener):
    # ISO 7089 - Form A washers (previously DIN 125A or just DIN 125)
    
    def __init__(self,d,material='SS',left_hand=False):
        
        super().__init__(d,material=material,left_hand=left_hand)
        
        self._standard = "ISO 7089"
        self._standard_full_name = "EN ISO 7089:2000"
        self._alternative_standards = ["DIN 125A","DIN 125"]
        self._product_name = 'Washer'
        with open(os.path.join(os.path.dirname(__file__),'includes','Washer.svg'),'r') as fid:
            self._image = fid.read()
    
    def get_dimensions(self):
        clearance_holes = {1.6:1.7,
                           2:2.2,
                           2.5:2.7,
                           3:3.2,
                           3.5:3.7,
                           4:4.3,
                           5:5.3,
                           6:6.4,
                           8:8.4,
                           10:10.5,
                           12:13,
                           14:15,
                           16:17,
                           18:19,
                           20:21,
                           22:23,
                           24:25,
                           27:28,
                           30:31,
                           33:34,
                           36:37,
                           39:42,
                           42:45,
                           45:48,
                           48:52,
                           52:56,
                           56:62,
                           60:66,
                           64:70}
        
        outside_diameters = {1.6:4,
                             2:5,
                             2.5:6,
                             3:7,
                             3.5:8,
                             4:9,
                             5:10,
                             6:12,
                             8:16,
                             10:20,
                             12:24,
                             14:28,
                             16:30,
                             18:34,
                             20:37,
                             22:39,
                             24:44,
                             27:50,
                             30:56,
                             33:60,
                             36:66,
                             39:72,
                             42:78,
                             45:85,
                             48:92,
                             52:98,
                             56:105,
                             60:110,
                             64:115}
        
        thickness = {1.6:0.3,
                     2:0.3,
                     2.5:0.5,
                     3:0.5,
                     3.5:0.5,
                     4:0.8,
                     5:1,
                     6:1.6,
                     8:1.6,
                     10:2,
                     12:2.5,
                     14:2.5,
                     16:3,
                     18:3,
                     20:3,
                     22:3,
                     24:4,
                     27:4,
                     30:4,
                     33:5,
                     36:5,
                     39:6,
                     42:8,
                     45:8,
                     48:8,
                     52:8,
                     56:10,
                     60:10,
                     64:10}
        
        dimensions = {'Nominal thread diameter':self._nom_diameter}

        if self._nom_diameter in clearance_holes:
            dimensions["Clearance hole (d_1)"] = clearance_holes[self._nom_diameter]
            dimensions["Outside diameter (d_2)"] = outside_diameters[self._nom_diameter]
            dimensions["Thickness (m)"] = thickness[self._nom_diameter]
        else:
            dimensions["Clearance hole (d_1)"] = "Not specified"
            dimensions["Outside diameter (d_2)"] = "Not specified"
            dimensions["Thickness (m)"] = "Not specified"
        return dimensions


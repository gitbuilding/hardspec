### Materials
class Material:
    
    def __init__(self,name,alt_names=None,short_name=None):
        self._name = name
        self._all_names = [self._name]
        
        if alt_names is None:
            self._alt_names = []
        elif isinstance(alt_names,str):
            self._alt_names = [alt_names]
        elif isinstance(alt_names,list):
            self._alt_names = alt_names
        else:
            raise('Wrong variable type')
        self._all_names += self._alt_names 
            
        if short_name is None:
            self._short_name = self._name
        else:
            self._short_name = short_name
            self._all_names += [short_name]
            
    def __str__(self):
        return self._name
            
    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._name.lower() == other._name.lower()
        elif isinstance(other, str):
            return other.lower() in [n.lower() for n in self._all_names]
        else:
            return False
    
    @property
    def name(self):
        return self._name
    
    @property
    def short_name(self):
        return self._short_name
    
    @property
    def alternative_names(self):
        return self._alt_names


Brass = Material('Brass')
SS = Material('A2 Stainless Steel',
      alt_names = ['SAE 304 Stainless Steel',
                   "18-8 Stainless Steel",
                   "UNS S30400 Stainless Steel",
                   "SUS304 Stainless Steel",
                   "European norm 1.4301 Stainless Steel"],
      short_name = 'SS')

materials = [Brass,SS]
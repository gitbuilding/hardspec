 
__author__ = 'Julian Stirling'

from setuptools import setup, find_packages
import sys
from os import path
import glob

if sys.version_info[0] == 2:
    sys.exit("Sorry, Python 2 is not supported")

#Globbing all of the non python files and then removing `hardspec/` from the start
package_data_location = glob.glob('hardspec/includes/**/*', recursive=True)
package_data_location = [package[9:] for package in package_data_location]

this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(name = 'hardspec',
      version = '0.0.1',
      description = 'Hardware specifications as defined by ISO standards',
      long_description = long_description,
      long_description_content_type='text/markdown',
      author = 'Julian Stirling',
      author_email = 'julian@julianstirling.co.uk',
      packages = find_packages(),
      
      package_data={'hardspec': package_data_location},
      keywords = ['Hardware','Standards'],
      zip_safe = True,
      url = 'https://gitlab.com/bath_open_instrumentation_group/hardspec',
      classifiers = [
          'Development Status :: 5 - Production/Stable',
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Programming Language :: Python :: 3.6'
          ],
      install_requires=[],
      python_requires=">=3.6",
      )


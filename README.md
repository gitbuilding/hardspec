
Hardware specifications as defined by ISO standards.

## What is `hardspec`?

`hardspec` is a library which contains specifications for hardware as defined by ISO standards. Such as the diameters and pitches of metric threads.

## How to use `hardspec`

Once `hardspec` is more mature it could be used by CAD software or hardware documentation software.

## What is the `hardspec` data format?

Most data from ISO standards is stored in python objects or class definitions. The format is currently a bit *ad hoc* as there is a lot of complex data to present. A better overview of the format will be written as the package matures.

## Which standards were used to write this

Where possible ISO standards have been used. The British versions from the BSI have been used, but the text should be identical for national versions of the international standard. Data has been extracted from the text, but not parts of the original text have been copied to avoid copyright issues.

## What is the `hardspec` licence?

`hardspec` is licensed under the MIT licence. Standards should be for everyone so a permissive licence is used. It is a pity that the official standards themselves are behind paywalls.